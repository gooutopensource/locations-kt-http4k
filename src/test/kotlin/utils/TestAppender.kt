package utils

import org.apache.logging.log4j.core.LogEvent
import org.apache.logging.log4j.core.appender.AbstractAppender

class TestAppender : AbstractAppender(TestAppender::class.simpleName, null, null, false, arrayOf()) {
    val events = mutableListOf<LogEvent>()
    override fun append(event: LogEvent?) {
        if (event != null) {
            events.add(event)
        }
    }

    override fun isStarted(): Boolean = true
}
