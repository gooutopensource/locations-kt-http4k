package config

import io.kotlintest.shouldBe
import net.goout.locations.config.notFoundThrower
import net.goout.locations.model.NotFoundException
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.core.then
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class NotFoundThrowerTest {
    @Test
    fun notFoundResponseIsThrownAsNotFoundException() {
        val handler = notFoundThrower().then(
            routes("/foo" bind Method.GET to { Response(OK) })
        )
        handler(Request(Method.GET, "/foo")).shouldBe(Response(OK))
        assertThrows<NotFoundException> {
            handler(Request(Method.GET, "/bar"))
        }
    }
}
