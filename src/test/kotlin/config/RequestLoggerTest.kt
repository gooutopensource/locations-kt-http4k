package config

import io.kotlintest.matchers.string.shouldMatch
import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import net.goout.locations.config.errorHandlerAndLogger
import net.goout.locations.model.BodyLens
import net.goout.locations.model.ErrorResponse
import net.goout.locations.model.NotFoundException
import net.goout.locations.util.log
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.Logger
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.core.then
import org.http4k.lens.LensFailure
import org.junit.jupiter.api.Test
import utils.TestAppender
import java.lang.RuntimeException

class RequestLoggerTest {
    @Test
    fun testRequestIsLogged() {
        val logSink = TestAppender()
        (log as Logger).addAppender(logSink)
        val method = Method.GET
        val path = "/path/to/hell"
        val remoteHost = "666.666.666.666"
        val userAgent = "Usually a complete mess"
        val status = Status.OK
        val request = Request(method, path)
            .header("X-Forwarded-For", remoteHost)
            .header("User-Agent", userAgent)

        val handler = errorHandlerAndLogger().then { Response(status) }
        handler(request)

        logSink.events.size.shouldBe(1)
        val expectedLog = "200 Request in \\d+ ms : $method $path : $remoteHost : $userAgent".toRegex()
        logSink.events.first().message.toString().shouldMatch(expectedLog)
        (log as Logger).removeAppender(logSink)
    }

    @Test
    fun notFoundExceptionsResultsIn404WithJsonWithMessage() {
        val message = "This message should be propagated to the response body"
        val handler = errorHandlerAndLogger().then {
            throw NotFoundException(message)
        }
        val response = handler(Request(Method.GET, "/"))

        response.status.shouldBe(Status.NOT_FOUND)
        BodyLens<ErrorResponse>()(response).shouldBe(ErrorResponse(message))
    }

    @Test
    fun lensFailureResultsIn400WithJsonWithMessage() {
        val handler = errorHandlerAndLogger().then {
            throw LensFailure(listOf(), null, null)
        }
        val response = handler(Request(Method.GET, "/"))

        response.status.shouldBe(Status.BAD_REQUEST)
        BodyLens<ErrorResponse>()(response).message.shouldNotBe(null)
    }

    @Test
    fun unknownExceptionsResultsIn500WithJsonWithoutMessage() {
        val handler = errorHandlerAndLogger().then {
            throw CustomException()
        }
        val response = handler(Request(Method.GET, "/"))

        response.status.shouldBe(Status.INTERNAL_SERVER_ERROR)
        BodyLens<ErrorResponse>()(response).shouldBe(ErrorResponse("Internal server error."))
    }

    @Test
    fun unknownExceptionsIsLoggedAsError() {
        val logSink = TestAppender()
        (log as Logger).addAppender(logSink)
        val handler = errorHandlerAndLogger().then {
            throw CustomException()
        }
        handler(Request(Method.GET, "/"))

        logSink.events.size.shouldBe(1)
        logSink.events.first().level.shouldBe(Level.FATAL)

        (log as Logger).removeAppender(logSink)
    }

    @Test
    fun todoErrorIsHandled() {
        val handler = errorHandlerAndLogger().then {
            TODO()
        }
        val response = handler(Request(Method.GET, "/"))

        response.status.shouldBe(Status.INTERNAL_SERVER_ERROR)
    }

    class CustomException : RuntimeException("foo")
}
