package controller

import io.kotlintest.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.goout.locations.controller.liveness
import net.goout.locations.controller.readiness
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.create
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.net.ConnectException

class HealthCheckControllerTest {
    @Test
    fun livenessReturnsOK() {
        val handler = liveness()
        handler(Request(Method.GET, "/liveness")).shouldBe(Response(OK))
    }

    @Test
    fun readinessReturnsOkWhenElasticIsOK() {
        val elasticClient = mockk<RestHighLevelClient>()
        every { elasticClient.ping(RequestOptions.DEFAULT) } returns true
        val handler = readiness(elasticClient)
        handler(Request(Method.GET, "/readiness")).shouldBe(Response(OK))
        verify { elasticClient.ping(RequestOptions.DEFAULT) }
    }

    @Test
    fun readinessThrowsWhenElasticIsNotReady() {
        val elasticClient = mockk<RestHighLevelClient>()
        every { elasticClient.ping(RequestOptions.DEFAULT) } returns false
        val handler = readiness(elasticClient)
        assertThrows<RuntimeException> {
            handler(Request(Method.GET, "/readiness"))
        }
        verify { elasticClient.ping(RequestOptions.DEFAULT) }
    }

    @Test
    fun readinessThrowsWhenElasticIsUnreachable() {
        val elasticClient = create("i.hope.this.host.will.never.work", 12345)
        val handler = readiness(elasticClient)
        assertThrows<ConnectException> {
            handler(Request(Method.GET, "/readiness"))
        }
    }
}
