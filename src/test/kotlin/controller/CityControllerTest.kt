package controller

import io.kotlintest.shouldBe
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.goout.locations.controller.getAssociatedFeaturedCity
import net.goout.locations.controller.getCityById
import net.goout.locations.controller.getClosestCity
import net.goout.locations.controller.getFeaturedCities
import net.goout.locations.model.CityResponse
import net.goout.locations.model.Coordinates
import net.goout.locations.model.ElasticCity
import net.goout.locations.model.ElasticRegion
import net.goout.locations.model.FeaturedCity
import net.goout.locations.model.JsonResponse
import net.goout.locations.model.MultiCityResponse
import net.goout.locations.model.NotFoundException
import net.goout.locations.model.Ok
import net.goout.locations.repository.CityAndRegionRepository
import org.http4k.contract.ContractRoute
import org.http4k.contract.contract
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class CityControllerTest {
    private val repository: CityAndRegionRepository = mockk()
    private val dummyElasticCity = ElasticCity(101748113, 666, "CZ", true, Coordinates(1.2, 3.4)).apply { names.put("name.cs", "Praha") }
    private val dummyElasticRegion = ElasticRegion(666).apply { names.put("name.cs", "Hlavní město Praha") }
    private val dummyCityResponse = CityResponse(101748113, true, "CZ", "Praha", "Hlavní město Praha")

    private fun toHandler(route: (CityAndRegionRepository) -> ContractRoute) = contract { routes += route(repository) }

    @BeforeEach
    fun setUpFreshRepository() {
        clearMocks(repository)
    }

    @Test
    fun featuredCityIsReturnedRightAway() {
        val handler = toHandler(::getAssociatedFeaturedCity)
        every { repository.getCityById("101748113") } returns dummyElasticCity
        every { repository.getRegionById("666") } returns dummyElasticRegion
        handler(Request(Method.GET, "/city/v1/associatedFeatured?language=cs&id=101748113"))
            .shouldBe(Ok(dummyCityResponse))
        verify(exactly = 1) { repository.getCityById("101748113") }
    }

    @Test
    fun nonFeaturedCityIsAssociatedAFeaturedOne() {
        val handler = toHandler(::getAssociatedFeaturedCity)
        val dummyCoordinates = Coordinates(4.3, -2.1)
        every { repository.getCityById("33") } returns ElasticCity(33, 44, "CZ", false, dummyCoordinates)
        every { repository.getClosestCity(dummyCoordinates, true) } returns dummyElasticCity
        every { repository.getRegionById("666") } returns dummyElasticRegion
        handler(Request(Method.GET, "/city/v1/associatedFeatured?language=cs&id=33"))
            .shouldBe(Ok(dummyCityResponse))
    }

    @Test
    fun invalidCityIdIsRejectedWith404() {
        val handler = toHandler(::getAssociatedFeaturedCity)
        every { repository.getCityById("33") } returns null
        assertThrows<NotFoundException> {
            handler(Request(Method.GET, "/city/v1/associatedFeatured?language=cs&id=33"))
        }
    }

    @Test
    fun cityIdIsParamRequired() {
        val handler = toHandler(::getCityById)
        handler(Request(Method.GET, "/city/v1/get?language=cs")).shouldBe(Response(Status.BAD_REQUEST))
    }

    @Test
    fun languageParamIsRequired() {
        val handler = toHandler(::getCityById)
        handler(Request(Method.GET, "/city/v1/get?id=123")).shouldBe(Response(Status.BAD_REQUEST))
    }

    @Test
    fun getCityById() {
        every { repository.getCityById("123") } returns null
        val handler = toHandler(::getCityById)
        assertThrows<NotFoundException> {
            handler(Request(Method.GET, "/city/v1/get?language=en&id=123"))
        }
        verify { repository.getCityById("123") }
    }

    @Test
    fun extraParamIsIgnored() {
        every { repository.getCityById("123") } returns null
        val handler = toHandler(::getCityById)
        assertThrows<NotFoundException> {
            handler(Request(Method.GET, "/city/v1/get?language=en&id=123&extra=shouldBeIgnored"))
        }
        verify { repository.getCityById("123") }
    }

    @Test
    fun latitudeCannotBeOutOfBounds() {
        val handler = toHandler(::getClosestCity)
        listOf(-100, 100).forEach { latitude ->
            handler(Request(Method.GET, "/city/v1/closest?language=en&lat=$latitude&lon=0"))
                .shouldBe(Response(Status.BAD_REQUEST))
        }
    }

    @Test
    fun longitudeCannotBeOutOfBounds() {
        val handler = toHandler(::getClosestCity)
        listOf(-200, 200).forEach { longitude ->
            handler(Request(Method.GET, "/city/v1/closest?language=en&lat=0&lon=$longitude"))
                .shouldBe(Response(Status.BAD_REQUEST))
        }
    }

    @Test
    fun latitudeMustBeDefinedAlongLongitude() {
        val handler = toHandler(::getClosestCity)
        handler(Request(Method.GET, "/city/v1/closest?language=en&lon=24"))
            .shouldBe(Response(Status.BAD_REQUEST))
    }

    @Test
    fun longitudeMustBeDefinedAlongLatitude() {
        val handler = toHandler(::getClosestCity)
        handler(Request(Method.GET, "/city/v1/closest?language=en&lat=24"))
            .shouldBe(Response(Status.BAD_REQUEST))
    }

    @Test
    fun fastlyGeolocationHeadersAreUsed() {
        every { repository.getCityByCoordinates(Coordinates(-24.55589, 50.0001), true) } returns dummyElasticCity
        every { repository.getRegionById("666") } returns dummyElasticRegion
        val handler = toHandler(::getClosestCity)
        val request = Request(Method.GET, "/city/v1/closest?language=cs")
            .header("Fastly-Geo-Lat", "-24.55589")
            .header("Fastly-Geo-Lon", "50.0001")
        handler(request).shouldBe(Ok(dummyCityResponse))
    }

    @Test
    fun fastlyCoordinatesAreIgnoredWhenBothAreZero() {
        every { repository.getCityById("${FeaturedCity.PRAGUE.id}") } returns dummyElasticCity
        every { repository.getRegionById("666") } returns dummyElasticRegion
        val handler = toHandler(::getClosestCity)
        val request = Request(Method.GET, "/city/v1/closest?language=cs")
            .header("Fastly-Geo-Lat", "0.000")
            .header("Fastly-Geo-Lon", "0.000")
        handler(request).shouldBe(Ok(dummyCityResponse))
    }

    @Test
    fun getFeaturedCities() {
        every { repository.getFeaturedCities() } returns listOf(dummyElasticCity)
        every { repository.getRegionById("666") } returns dummyElasticRegion
        val expected = JsonResponse(Status.OK, MultiCityResponse(listOf(dummyCityResponse)))
        val handler = toHandler(::getFeaturedCities)
        handler(Request(Method.GET, "/city/v1/featured?language=cs")).shouldBe(expected)
    }
}
