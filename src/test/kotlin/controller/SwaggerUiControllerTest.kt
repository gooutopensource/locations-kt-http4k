package controller

import io.kotlintest.shouldBe
import net.goout.locations.controller.swaggerUi
import org.http4k.core.Method
import org.http4k.core.Status
import org.junit.jupiter.api.Test

class SwaggerUiControllerTest {
    @Test
    fun swaggerUiIsAvailable() {
        val handler = swaggerUi("jede liska k Taboru")
        val response = handler(org.http4k.core.Request(Method.GET, "/docs/index.html"))
        response.status.shouldBe(Status.OK)
    }
}
