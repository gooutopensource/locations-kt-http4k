package integration

import io.kotlintest.shouldBe
import net.goout.locations.app
import net.goout.locations.controller.swaggerUi
import net.goout.locations.model.CityResponse
import net.goout.locations.model.Language
import net.goout.locations.model.MultiCityResponse
import net.goout.locations.model.Ok
import org.elasticsearch.client.create
import org.http4k.core.Body
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Status
import org.http4k.format.Jackson.auto
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import org.junit.jupiter.params.provider.ValueSource

class LocationsIT {
    private val elasticClient = create(
        System.getenv("GOOUT_ELASTIC_HOST"),
        System.getenv("GOOUT_ELASTIC_PORT").toInt()
    )

    val app = app(elasticClient)

    @ParameterizedTest
    @EnumSource(Language::class)
    fun eachLanguageMustHaveAFallbackCity(language: Language) {
        val request = Request(Method.GET, "/city/v1/closest?language=${language.name.toLowerCase()}")
        app(request).status.shouldBe(Status.OK)
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            "/liveness",
            "/readiness",
            "/docs/index.html",
            "/city/v1/get?id=101909779&language=cs",
            "/city/v1/closest?language=cs",
            "/city/v1/featured?language=cs",
            "/city/v1/search?query=nejnezpravděpodobnostňovávatelnější&language=cs",
            "/city/v1/associatedFeatured?id=101909779&language=cs"
        ]
    )
    fun pathRespondsWith200(path: String) {
        app(Request(Method.GET, path)).status.shouldBe(Status.OK)
    }

    @Test
    fun docsRedirectToSwaggerUi() {
        val serviceName = System.getenv("SERVICE_NAME")
        val serviceRoot = if (serviceName != null) "/services/$serviceName" else ""
        val handler = swaggerUi("/zap")
        val response = handler(Request(Method.GET, "/docs"))
        response.status.shouldBe(Status.FOUND)
        response.header("Location").shouldBe("$serviceRoot/docs/index.html?url=$serviceRoot/zap")
    }

    @Test
    fun intersectionIsPreferredOverProximity() {
        // we expect Prague even though Hostavice are closer
        val expected = Ok(CityResponse(101748113, true, "CZ", "Praga", "Praga"))
        val response = app(Request(Method.GET, "/city/v1/closest?language=pl&lat=50.107&lon=14.574"))
        response.shouldBe(expected)
    }

    @Test
    fun correctDiacriticsIsPreferredOverPopulation() {
        val response = app(Request(Method.GET, "/city/v1/search?language=cs&query=křemže"))
        val parsedCities = Body.auto<MultiCityResponse>().toLens()(response)
        parsedCities
            .cities
            .take(2)
            .map { it.name }
            // assert that correct diacritics boost match
            .shouldBe(listOf("Křemže", "Kremže"))
    }

    @Test
    fun searchingWithoutDiacriticsPrefersLargerCities() {
        val response = app(Request(Method.GET, "/city/v1/search?language=cs&query=kremze"))
        val parsedCities = Body.auto<MultiCityResponse>().toLens()(response)
        parsedCities
            .cities
            .take(2)
            .map { it.name }
            // assert that ascii-only match orders by population
            .shouldBe(listOf("Kremže", "Křemže"))
    }
}
