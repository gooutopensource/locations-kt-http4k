package net.goout.locations

import net.goout.locations.config.errorHandlerAndLogger
import net.goout.locations.config.notFoundThrower
import net.goout.locations.controller.getAssociatedFeaturedCity
import net.goout.locations.controller.getCityById
import net.goout.locations.controller.getClosestCity
import net.goout.locations.controller.getFeaturedCities
import net.goout.locations.controller.liveness
import net.goout.locations.controller.readiness
import net.goout.locations.controller.search
import net.goout.locations.controller.swaggerUi
import net.goout.locations.repository.ElasticCityAndRegionRepository
import org.elasticsearch.client.RestHighLevelClient
import org.http4k.contract.contract
import org.http4k.contract.openapi.ApiInfo
import org.http4k.contract.openapi.v3.OpenApi3
import org.http4k.core.HttpHandler
import org.http4k.core.then
import org.http4k.format.Jackson
import org.http4k.routing.routes

private const val API_DESCRIPTION_PATH = "/api-docs"

fun app(elasticClient: RestHighLevelClient): HttpHandler {
    val cityAndRegionRepository = ElasticCityAndRegionRepository(elasticClient)

    val api = contract {
        renderer = OpenApi3(ApiInfo("GoOut Locations API", "1.0"), Jackson)
        descriptionPath = API_DESCRIPTION_PATH
        routes += getAssociatedFeaturedCity(cityAndRegionRepository)
        routes += getCityById(cityAndRegionRepository)
        routes += getClosestCity(cityAndRegionRepository)
        routes += getFeaturedCities(cityAndRegionRepository)
        routes += search(cityAndRegionRepository)
    }

    // catch exceptions, translate them to custom responses, also log all incoming requests
    return errorHandlerAndLogger()
        // necessary to attach custom response to 404s
        .then(notFoundThrower())
        // register routes
        .then(
            routes(
                readiness(elasticClient),
                liveness(),
                swaggerUi(API_DESCRIPTION_PATH),
                api
            )
        )
}
