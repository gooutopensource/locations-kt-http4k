package net.goout.locations.util

import org.http4k.lens.BiDiLensSpec
import org.http4k.lens.Lens
import org.http4k.lens.LensFailure
import org.http4k.lens.Missing

/**
 * Asserts the receiver is within given Range.
 * If so, the receiver is returned.
 * If not, IllegalArgumentException is thrown.
 */
private fun <T : Comparable<T>> T.assertInRange(range: ClosedRange<T>): T {
    if (this !in range) {
        throw IllegalArgumentException("$this is outside of $range")
    }
    return this
}

/**
 * Narrows down the receiver Lens to given Range.
 * If the extracted or injected value is outside the Range the lens will fail.
 */
fun <IN : Any, OUT : Comparable<OUT>> BiDiLensSpec<IN, OUT>.bounded(range: ClosedRange<OUT>): BiDiLensSpec<IN, OUT> =
    map({ it.assertInRange(range) }, { it.assertInRange(range) })

/**
 * For a Pair of optional lenses Lens<IN, F?> and Lens<IN, S?> extracts a nullable Pair of F and S only if both lenses extract a value.
 * Null is returned if both lenses extract null.
 * LensFailure is thrown if one lens extracts null and the other extracts a value.
 */
operator fun <IN : Any, F : Any?, S : Any?> Pair<Lens<IN, F?>, Lens<IN, S?>>.invoke(target: IN): Pair<F, S>? {
    val firstValue = first(target)
    val secondValue = second(target)

    return when {
        firstValue == null && secondValue == null -> null
        firstValue == null -> throw LensFailure(Missing(first.meta), target = target)
        secondValue == null -> throw LensFailure(Missing(second.meta), target = target)
        else -> Pair(firstValue, secondValue)
    }
}
