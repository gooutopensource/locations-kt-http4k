package net.goout.locations.controller

import org.http4k.core.Method
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.routing.ResourceLoader.Companion.Classpath
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.routing.static

// Hack to make this work locally as well as in cloud where the public path is prefixed with "/services/<service name>"
val SERVICE_NAME: String? = System.getenv("SERVICE_NAME")
val SERVICE_ROOT: String = if (SERVICE_NAME != null) "/services/$SERVICE_NAME" else ""

/**
 * Exposes Swagger UI with "/docs" path as its entry point.
 * Note that this handler assumes to be bound on the "/" root path of the whole service.
 * @param descriptionPath absolute path to API description JSON. The UI will be configured to fetch it after load.
 */
fun swaggerUi(descriptionPath: String): RoutingHttpHandler = routes(
    "/docs" bind Method.GET to {
        Response(Status.FOUND).header("Location", "$SERVICE_ROOT/docs/index.html?url=$SERVICE_ROOT$descriptionPath")
    },
    "/docs" bind static(Classpath("META-INF/resources/webjars/swagger-ui/3.25.2"))
)
