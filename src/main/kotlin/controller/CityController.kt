package net.goout.locations.controller

import net.goout.locations.model.CityResponse
import net.goout.locations.model.Coordinates
import net.goout.locations.model.ElasticCity
import net.goout.locations.model.ErrorResponse
import net.goout.locations.model.FeaturedCity
import net.goout.locations.model.Language
import net.goout.locations.model.MultiCityResponse
import net.goout.locations.model.NotFoundException
import net.goout.locations.model.Ok
import net.goout.locations.model.exampleCityResponse
import net.goout.locations.model.exampleMultiCityResponse
import net.goout.locations.model.language
import net.goout.locations.model.nameKey
import net.goout.locations.model.returningJson
import net.goout.locations.repository.CityAndRegionRepository
import net.goout.locations.util.bounded
import net.goout.locations.util.invoke
import org.http4k.contract.ContractRoute
import org.http4k.contract.meta
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Status
import org.http4k.lens.BiDiLens
import org.http4k.lens.Header
import org.http4k.lens.LensFailure
import org.http4k.lens.Query
import org.http4k.lens.double
import org.http4k.lens.long
import org.http4k.lens.nonEmptyString
import org.http4k.lens.string

fun getAssociatedFeaturedCity(repository: CityAndRegionRepository): ContractRoute {
    val openApiSpec = "/city/v1/associatedFeatured" meta {
        summary = "For a given cityId returns the closest featured city. To be used mainly by Activity Feed."
        queries += idQuery
        queries += languageQuery
        returningJson(Status.OK, exampleCityResponse)
        returningJson(Status.BAD_REQUEST, ErrorResponse("Missing or malformed parameter."))
        returningJson(Status.NOT_FOUND, ErrorResponse("City with given id does not exist."))
    } bindContract Method.GET

    return openApiSpec to { request ->
        val id = idQuery(request)
        val language = languageQuery(request)

        val city = repository.getCityById("$id") ?: throw NotFoundException("City#$id does not exist.")
        val associatedCity = if (city.isFeatured == true) city else repository.getClosestCity(city.centroid, true)

        Ok(associatedCity.createResponse(language, repository))
    }
}

fun getCityById(repository: CityAndRegionRepository): ContractRoute {
    val openApiSpec = "/city/v1/get" meta {
        summary = "Get city of given ID localized to given language."
        queries += idQuery
        queries += languageQuery
        returningJson(Status.OK, exampleCityResponse)
        returningJson(Status.BAD_REQUEST, ErrorResponse("Missing or malformed parameter."))
        returningJson(Status.NOT_FOUND, ErrorResponse("City with given id does not exist."))
    } bindContract Method.GET

    return openApiSpec to { request: Request ->
        val id = idQuery(request)
        val language = languageQuery(request)

        val city = repository.getCityById("$id") ?: throw NotFoundException("City#$id does not exist.")

        Ok(city.createResponse(language, repository))
    }
}

fun getClosestCity(repository: CityAndRegionRepository): ContractRoute {
    val coordinatesQuery = Pair(
        Query.double().bounded(-90.0..90.0).optional("lat", "latitude in decimal degrees with . as decimal separator"),
        Query.double().bounded(-180.0..180.0).optional("lon", "longitude in decimal degrees with . as decimal separator")
    )

    val openApiSpec = "/city/v1/closest" meta {
        summary = "Returns a single city that is closest to the coordinates. If coordinates are not given we fallback to IP geo-location to find the closest featured city."
        queries += coordinatesQuery.toList()
        queries += languageQuery
        returningJson(Status.OK, exampleCityResponse)
        returningJson(Status.BAD_REQUEST, ErrorResponse("Parameters 'lat' and 'lon' are not given as pair or are malformed."))
    } bindContract Method.GET

    return openApiSpec to { request: Request ->
        val language = languageQuery(request)

        // Use location provided by user.
        val userCoordinates = coordinatesQuery(request)
        if (userCoordinates != null) {
            val city = repository.getCityByCoordinates(Coordinates(userCoordinates.first, userCoordinates.second))
            return@to Ok(city.createResponse(language, repository))
        }

        // Fallback to location approximated from IP address.
        val ipCoordinates = getIpCoordinates(request)
        if (ipCoordinates != null) {
            // Precision of IP geo-location is low so we only search for featured cities.
            val city = repository.getCityByCoordinates(ipCoordinates, true)
            return@to Ok(city.createResponse(language, repository))
        }

        // Fallback to city inferred from language parameter.
        val featuredCity = when (language) {
            Language.CS -> FeaturedCity.PRAGUE
            Language.DE -> FeaturedCity.BERLIN
            Language.EN -> FeaturedCity.PRAGUE
            Language.PL -> FeaturedCity.WARSAW
            Language.SK -> FeaturedCity.BRATISLAVA
        }
        val city = repository.getCityById("${featuredCity.id}") ?: throw RuntimeException("FeaturedCity[$featuredCity] could not be found.")
        Ok(city.createResponse(language, repository))
    }
}

fun getFeaturedCities(repository: CityAndRegionRepository): ContractRoute {
    val openApiSpec = "/city/v1/featured" meta {
        summary = "Returns a list of all featured cities. Order of the list is indicative and should be translated to UI. The list might be empty."
        queries += languageQuery
        returningJson(Status.OK, exampleMultiCityResponse)
        returningJson(Status.BAD_REQUEST, ErrorResponse("Missing or malformed language parameter."))
    } bindContract Method.GET

    return openApiSpec to { request: Request ->
        val language = languageQuery(request)
        // TODO: once migrated from Localities, infer the preferred order from eg. user's location
        val preferredCountryIso = when (language) {
            Language.CS -> "CZ"
            Language.DE -> "DE"
            Language.EN -> "CZ"
            Language.PL -> "PL"
            Language.SK -> "SK"
        }

        val cities = repository.getFeaturedCities()
            .sortedByDescending { it.countryIso == preferredCountryIso }
            .map { it.createResponse(language, repository) }

        Ok(MultiCityResponse(cities))
    }
}

fun search(repository: CityAndRegionRepository): ContractRoute {
    val queryQuery = Query.string().required("query", "the search query")
    val countryIsoQuery = Query.string().nonEmptyString()
        .optional("countryIso", "ISO 3166-1 alpha-2 country code. Can be used to limit scope of the search to a given country.")

    val openApiSpec = "/city/v1/search" meta {
        summary =
            """
            Returns list of cities matching the 'query' parameter.
            The response is limited to 10 cities and no pagination is provided.
            We believe we will return the correct result at the top.
            Otherwise the user have to provide more specific query string.
            The list is not guaranteed to be non-empty even with 200 response code.
            """.trimIndent()
        queries += languageQuery
        queries += queryQuery
        queries += countryIsoQuery
        returningJson(Status.OK, exampleMultiCityResponse)
        returningJson(Status.BAD_REQUEST, ErrorResponse("The query string is not given or is empty or missing or malformed language parameter."))
    } bindContract Method.GET

    return openApiSpec to { request ->
        val language = languageQuery(request)
        val query = queryQuery(request)
        val countryIso = countryIsoQuery(request)

        val cities = repository.search(query, language, countryIso)
            .map { it.createResponse(language, repository) }

        Ok(MultiCityResponse(cities))
    }
}

val idQuery: BiDiLens<Request, Long> = Query.long().required("id", "positive integer")
val languageQuery: BiDiLens<Request, Language> = Query.language().required("language", "two letter ISO 639-1 lowercase language code")

/**
 * Converts ElasticCity to ElasticResponse localized to given Language.
 * Throws RuntimeException if the city's region cannot be found.
 * Throws RuntimeException if either city or region does not contain a name in given Language.
 */
private fun ElasticCity.createResponse(language: Language, repository: CityAndRegionRepository): CityResponse {
    val region = repository.getRegionById("$regionId") ?: throw RuntimeException("City#$id links non existent Region#$regionId")
    return CityResponse(
        id = id,
        isFeatured = isFeatured ?: false,
        countryIso = countryIso,
        name = names[language.nameKey] ?: throw RuntimeException("City#$id is missing a name in Language[$language]"),
        regionName = region.names[language.nameKey] ?: throw RuntimeException("Region#${region.id} is missing name in Language[$language]")
    )
}

private fun getIpCoordinates(request: Request): Coordinates? {
    try {
        val coordinatesHeader = Pair(
            Header.double().bounded(-90.0..90.0).optional("Fastly-Geo-Lat"),
            Header.double().bounded(-180.0..180.0).optional("Fastly-Geo-Lon")
        )
        val (lat, lon) = coordinatesHeader(request) ?: return null
        // dummy value returned by Fastly for reserved IP blocks
        // https://docs.fastly.com/vcl/geolocation/#reserved-ip-address-blocks
        if (lat == 0.0 && lon == 0.0) {
            return null
        }
        return Coordinates(lat, lon)
    } catch (e: LensFailure) {
        // LensFailure is translated to BAD_REQUEST response,
        // but since the headers are not set by client but by Fastly, which is considered a part of the backend,
        // we rethrow a RunTimeException to respond with INTERNAL_SERVER_ERROR.
        throw RuntimeException("Invalid Fastly Geolocation headers", e)
    }
}
