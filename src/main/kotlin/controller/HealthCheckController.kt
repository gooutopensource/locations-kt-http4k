package net.goout.locations.controller

import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.http4k.core.Method
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind

/**
 * Indicates the service and its downstream dependencies are ready to accept traffic by returning HTTP 200.
 */
fun readiness(elasticClient: RestHighLevelClient): RoutingHttpHandler = "/readiness" bind Method.GET to {
    if (!elasticClient.ping(RequestOptions.DEFAULT)) {
        throw RuntimeException("Elastic not available.")
    }
    Response(OK)
}

/**
 * Indicates the service is running by returning HTTP 200.
 */
fun liveness(): RoutingHttpHandler = "/liveness" bind Method.GET to { Response(OK) }
