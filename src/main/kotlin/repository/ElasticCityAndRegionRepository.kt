package net.goout.locations.repository

import com.fasterxml.jackson.databind.ObjectMapper
import io.inbot.eskotlinwrapper.IndexRepository
import io.inbot.eskotlinwrapper.JacksonModelReaderAndWriter
import io.inbot.eskotlinwrapper.SearchResults
import net.goout.locations.model.Coordinates
import net.goout.locations.model.ElasticCity
import net.goout.locations.model.ElasticRegion
import net.goout.locations.model.Language
import net.goout.locations.model.nameKey
import org.elasticsearch.action.search.source
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.indexRepository
import org.elasticsearch.common.geo.ShapeRelation
import org.elasticsearch.common.lucene.search.function.FieldValueFactorFunction
import org.elasticsearch.geometry.Point
import org.elasticsearch.index.query.BoolQueryBuilder
import org.elasticsearch.index.query.GeoShapeQueryBuilder
import org.elasticsearch.index.query.MatchAllQueryBuilder
import org.elasticsearch.index.query.MultiMatchQueryBuilder
import org.elasticsearch.index.query.TermQueryBuilder
import org.elasticsearch.index.query.functionscore.FieldValueFactorFunctionBuilder
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder
import org.elasticsearch.search.fetch.subphase.FetchSourceContext
import org.elasticsearch.search.sort.FieldSortBuilder
import org.elasticsearch.search.sort.GeoDistanceSortBuilder
import org.elasticsearch.search.sort.SortOrder
import java.util.concurrent.ConcurrentHashMap

class ElasticCityAndRegionRepository(elasticClient: RestHighLevelClient) : CityAndRegionRepository {
    companion object {
        private const val REGION_INDEX = "region"
        private const val CITY_INDEX = "city"
        private inline fun <reified T : Any> RestHighLevelClient.repository(index: String, ignoredFields: Array<String>): IndexRepository<T> =
            indexRepository(
                index,
                fetchSourceContext = FetchSourceContext(true, null, ignoredFields),
                // We must override the default because it uses SNAKE_CASE PropertyNamingStrategy.
                modelReaderAndWriter = JacksonModelReaderAndWriter.create(ObjectMapper().findAndRegisterModules())
            )
    }

    private val cityRepository = elasticClient.repository<ElasticCity>(CITY_INDEX, arrayOf("geometry", "timezone"))
    private val regionRepository = elasticClient.repository<ElasticRegion>(REGION_INDEX, arrayOf("centroid", "geometry", "timezone"))
    private val regionCache = ConcurrentHashMap<String, ElasticRegion>()

    override fun getCityById(id: String): ElasticCity? = cityRepository.get(id)

    override fun getRegionById(id: String): ElasticRegion? {
        if (regionCache[id] != null) {
            return regionCache[id]
        }
        val region = regionRepository.get(id)
        if (region != null) {
            regionCache[id] = region
        }
        return region
    }

    override fun getFeaturedCities(): List<ElasticCity> = cityRepository.search {
        source {
            query(
                TermQueryBuilder("isFeatured", true)
            )
            sort(
                FieldSortBuilder("countryIso")
            )
            sort(
                FieldSortBuilder("population").apply {
                    order(SortOrder.DESC)
                }
            )
            // This assumes we do not have more than 1000 featured cities.
            size(1000)
        }
    }.getAll()

    override fun search(query: String, language: Language, countryIso: String?): List<ElasticCity> = cityRepository.search {
        source {
            query(
                FunctionScoreQueryBuilder(
                    BoolQueryBuilder().apply {
                        must(
                            // Inspired by https://www.elastic.co/guide/en/elasticsearch/reference/current/search-as-you-type.html
                            MultiMatchQueryBuilder(query).apply {
                                fields(
                                    mapOf(
                                        // Match against the specified language with diacritics.
                                        // Use the highest boost (8) because these three fields are most specific.
                                        "${language.nameKey}.autocomplete" to 8f,
                                        "${language.nameKey}.autocomplete._2gram" to 8f,
                                        "${language.nameKey}.autocomplete._3gram" to 8f,
                                        // Match against ascii versions of the name to match queries without diacritics.
                                        // Lower boost by factor of two, to prefer cities that matched with diacritics.
                                        "${language.nameKey}.autocomplete_ascii" to 4f,
                                        "${language.nameKey}.autocomplete_ascii._2gram" to 4f,
                                        "${language.nameKey}.autocomplete_ascii._3gram" to 4f,
                                        // Match against all language mutations with diacritics.
                                        // Lower the boost by factor of 4 to prefer matches in specified language.
                                        "name.all.autocomplete" to 2f,
                                        "name.all.autocomplete._2gram" to 2f,
                                        "name.all.autocomplete._3gram" to 2f,
                                        // Match against ascii version of all language mutations.
                                        // Lower the boost by factor of 8 because this is the least specific field.
                                        "name.all.autocomplete_ascii" to 1f,
                                        "name.all.autocomplete_ascii._2gram" to 1f,
                                        "name.all.autocomplete_ascii._3gram" to 1f
                                    )
                                )
                                type(MultiMatchQueryBuilder.Type.BOOL_PREFIX)
                            }
                        )
                        if (!countryIso.isNullOrBlank()) {
                            filter(
                                TermQueryBuilder("countryIso", countryIso)
                            )
                        }
                    },
                    // Boost cities with higher population.
                    FieldValueFactorFunctionBuilder("population").apply {
                        // Take logarithm of the city's population to account for human's logarithmic perception of size.
                        // Add 2 before taking the logarithm to make the score function strictly positive,
                        // because it's multiplied with the MultiMatch score.
                        modifier(FieldValueFactorFunction.Modifier.LN2P)
                        // For missing values assume 500 humans live there.
                        // Based on https://gitlab.com/GoOutNet/Backend/-/merge_requests/2589#note_336613844
                        missing(500.0)
                    }
                )
            )
            size(10)
        }
    }.getAll()

    override fun getCityByCoordinates(coordinates: Coordinates, featured: Boolean?): ElasticCity =
        getIntersectingCity(coordinates, featured)
            ?: getClosestCity(coordinates, featured)

    override fun getClosestCity(coordinates: Coordinates, featured: Boolean?): ElasticCity = cityRepository.search {
        source {
            query(
                if (featured != null) {
                    TermQueryBuilder("isFeatured", featured)
                } else {
                    MatchAllQueryBuilder()
                }
            )
            sort(GeoDistanceSortBuilder("centroid", coordinates.lat, coordinates.lon))
            size(1)
        }
    }.getSingle() ?: throw RuntimeException("Closest City could not be found for $coordinates, featured[$featured]")

    private fun getIntersectingCity(coordinates: Coordinates, featured: Boolean?): ElasticCity? = cityRepository.search {
        source {
            query(
                BoolQueryBuilder().apply {
                    filter(
                        // The Point accepts lat/lon in reverse order.
                        GeoShapeQueryBuilder("geometry", Point(coordinates.lon, coordinates.lat)).apply {
                            relation(ShapeRelation.INTERSECTS)
                        }
                    )
                    if (featured != null) {
                        filter(
                            TermQueryBuilder("isFeatured", featured)
                        )
                    }
                }
            )
            size(1)
        }
    }.getSingle()

    private fun <T : Any> SearchResults<T>.getSingle(): T? = mappedHits.firstOrNull()

    private fun <T : Any> SearchResults<T>.getAll(): List<T> = mappedHits.toList()
}
