package net.goout.locations.repository

import net.goout.locations.model.Coordinates
import net.goout.locations.model.ElasticCity
import net.goout.locations.model.ElasticRegion
import net.goout.locations.model.Language

interface CityAndRegionRepository {
    /**
     * Returns city of given id or null if city with given id cannot be not found.
     */
    fun getCityById(id: String): ElasticCity?

    /**
     * Returns region of given id or null if region with given id cannot be not found.
     */
    fun getRegionById(id: String): ElasticRegion?

    /**
     * Returns a List of cities which have `isFeatured` set to `true`.
     * Returns empty List when no such cities are found.
     * The returned cities are ordered by country and by population within each country.
     */
    fun getFeaturedCities(): List<ElasticCity>

    /**
     * Given a Coordinates looks for a city that geospatially contains the them.
     * If no such city can be found, falls back to finding the nearest city measured by distance from the coordinates to city centroid.
     * @param featured Can be used to narrow down the search to (non) featured cities.
     */
    fun getCityByCoordinates(coordinates: Coordinates, featured: Boolean? = null): ElasticCity

    /**
     * Given a Coordinates looks for the nearest city measured by distance from the coordinates to city centroid.
     * @param featured Can be used to narrow down the search to (non) featured cities.
     */
    fun getClosestCity(coordinates: Coordinates, featured: Boolean? = null): ElasticCity

    /**
     * Performs a fulltext search for given query parameter and returns a list of 10 (or less) best matching cities.
     * @param query the search query
     * @param language improves the search results by specifying language of the query param
     * @param countryIso can be specified to narrow down the search to particular country
     */
    fun search(query: String, language: Language, countryIso: String?): List<ElasticCity>
}
