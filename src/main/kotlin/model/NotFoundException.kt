package net.goout.locations.model

class NotFoundException(message: String) : RuntimeException(message)
