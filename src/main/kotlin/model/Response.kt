package net.goout.locations.model

import org.http4k.contract.RouteMetaDsl
import org.http4k.core.Body
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.core.Status.Companion.OK
import org.http4k.core.with
import org.http4k.format.Jackson.auto
import org.http4k.lens.BiDiBodyLens

data class CityResponse(
    val id: Long,
    val isFeatured: Boolean,
    val countryIso: String,
    val name: String,
    val regionName: String
)

val exampleCityResponse = CityResponse(
    id = 123,
    isFeatured = false,
    countryIso = "CZ",
    name = "Plzeň",
    regionName = "Plzeňský kraj"
)

data class MultiCityResponse(
    val cities: List<CityResponse>
)

val exampleMultiCityResponse = MultiCityResponse(listOf(exampleCityResponse))

data class ErrorResponse(
    val message: String
)

// syntactic sweetening
inline fun <reified T : Any> BodyLens(): BiDiBodyLens<T> = Body.auto<T>().toLens()

inline fun <reified T : Any> Ok(body: T): Response = JsonResponse(OK, body)

inline fun <reified T : Any> JsonResponse(status: Status, body: T): Response = Response(status).with(BodyLens<T>() of body)

inline fun <reified T : Any> RouteMetaDsl.returningJson(status: Status, body: T): Unit = returning(status, BodyLens<T>() to body)
