package net.goout.locations.model

import com.fasterxml.jackson.annotation.JsonAnySetter
import net.goout.locations.util.invoke
import org.http4k.core.Request
import org.http4k.lens.BiDiLensSpec
import org.http4k.lens.BiDiMapping
import org.http4k.lens.Query
import org.http4k.lens.map

data class Coordinates(
    val lat: Double,
    val lon: Double
)

data class ElasticCity(
    val id: Long,
    val regionId: Long,
    val countryIso: String,
    val isFeatured: Boolean?,
    val centroid: Coordinates
) {
    // For now Jackson does not support this for constructor parameters https://github.com/FasterXML/jackson-databind/issues/562
    @JsonAnySetter
    val names: MutableMap<String, String> = mutableMapOf()
}

data class ElasticRegion(
    val id: Long
) {
    // For now Jackson does not support this for constructor parameters https://github.com/FasterXML/jackson-databind/issues/562
    @JsonAnySetter
    val names: MutableMap<String, String> = mutableMapOf()
}

enum class Language {
    CS,
    DE,
    EN,
    PL,
    SK
}

val Language.nameKey: String
    get() = "name.${name.toLowerCase()}"

// helper lens for (de)serialization of Language from/to lowercase query parameter
fun Query.language(): BiDiLensSpec<Request, Language> = map(BiDiMapping({ Language.valueOf(it.toUpperCase()) }, { it.name.toLowerCase() }))

/**
 * FeaturedCity is a City that represents a Locality which is a deprecated implementation of location within GoOut.
 * This set of named IDs is meant as a temporary solution to ease up the migration from Localities to Cities.
 */
enum class FeaturedCity(
    val id: Long
) {
    BERLIN(101909779),
    BRATISLAVA(1108800123),
    BRNO(101748109),
    CRACOW(101752117),
    OSTRAVA(101748125),
    PILSEN(101748111),
    POZNAN(101752223),
    PRAGUE(101748113),
    WARSAW(101752777),
    WROCLAW(101752181),
}
