package net.goout.locations

import net.goout.locations.util.log
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.create
import org.http4k.server.Jetty
import org.http4k.server.asServer
import java.net.ConnectException

fun main() {
    val elasticClient = create(
        System.getenv("GOOUT_ELASTIC_HOST"),
        System.getenv("GOOUT_ELASTIC_PORT").toInt()
    ).pingOrClose()

    val app = app(elasticClient)
        // we use Jetty as Netty may have some performance issues https://github.com/http4k/http4k/issues/141
        .asServer(Jetty(8080))
        // 🚀
        .start()

    log.info("Server listening on 8080.")
    app.block()
}

// If the ping fails we must explicitly close the client to kill all spawned threads. Otherwise the process will not exit.
private fun RestHighLevelClient.pingOrClose(): RestHighLevelClient {
    try {
        ping(RequestOptions.DEFAULT)
    } catch (e: ConnectException) {
        close()
        throw e
    }
    return this
}
