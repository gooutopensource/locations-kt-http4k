package net.goout.locations.config

import net.goout.locations.model.ErrorResponse
import net.goout.locations.model.JsonResponse
import net.goout.locations.model.NotFoundException
import net.goout.locations.util.log
import org.http4k.core.Filter
import org.http4k.core.HttpHandler
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.lens.LensFailure

/**
 * Catches all Throwables and translates them to appropriate Responses.
 * Also logs all requests.
 */
fun errorHandlerAndLogger() = Filter { next: HttpHandler ->
    { request: Request ->
        val start = System.currentTimeMillis()
        val (response, exception) = try {
            Pair(next(request), null)
        } catch (e: Throwable) {
            Pair(e.toResponse(), e)
        }
        val latency = System.currentTimeMillis() - start
        val message = "${response.status.code} Request in $latency ms : ${request.method} ${request.uri} : ${request.remoteAddress} : ${request.userAgent}"
        when {
            response.status.code < 400 -> log.info(message)
            response.status.code < 500 -> log.warn(message)
            else -> log.fatal(message, exception)
        }
        response
    }
}

val Request.remoteAddress: String
    get() = header("X-Forwarded-For")?.replace(",.*".toRegex(), "") ?: ""

val Request.userAgent: String
    get() = header("User-Agent") ?: ""

fun Throwable.toResponse(): Response = when (this) {
    is LensFailure -> JsonResponse(Status.BAD_REQUEST, ErrorResponse("$message ${cause?.message ?: ""}"))
    is NotFoundException -> JsonResponse(Status.NOT_FOUND, ErrorResponse(message ?: "Not found."))
    else -> JsonResponse(Status.INTERNAL_SERVER_ERROR, ErrorResponse("Internal server error."))
}
