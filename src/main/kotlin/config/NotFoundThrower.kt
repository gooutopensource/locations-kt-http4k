package net.goout.locations.config

import net.goout.locations.model.NotFoundException
import org.http4k.core.Filter
import org.http4k.core.HttpHandler
import org.http4k.core.Request
import org.http4k.core.Status

// In case of no matching route the http4k router returns 404 right away.
// We catch it here and throw our custom exception so it can be later caught by the handleErrors Filter,
// which will transform it to our custom JSON error response.
fun notFoundThrower() = Filter { next: HttpHandler ->
    { request: Request ->
        val response = next(request)
        if (response.status == Status.NOT_FOUND) {
            throw NotFoundException(response.bodyString().ifEmpty { "${request.uri.path} not found" })
        }
        response
    }
}
