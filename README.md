# locations-kt-http4k (Locations Service in Kotlin http4k)
This service manages Cities and Regions which represent real-world locations within GoOut infrastructure.

## Alternatives

Multiple implementations of this service exist in different frameworks, languages for comparison.

- [locations-kt-ktor](https://gitlab.com/gooutopensource/locations-kt-ktor) in Kotlin Ktor; less complete,
- [locations-rs](https://github.com/strohel/locations-rs) in Rust Actix; contains API spec and additional tools and data for testing, benchmarking.

## API
OpenApi3 documentation is exposed at `/api-docs`. Swagger UI for the documentation is exposed at `/docs`.

## Storage
Data are stored in Elastic. As of now this service is read-only and does not alter the data in any way.

Example structure (mappings) and data needed to run this service can be found in the [`elasticsearch` directory of the locations-rs repository](https://github.com/strohel/locations-rs/tree/master/elasticsearch).

## Running locally
- Make sure you have `GOOUT_ELASTIC_HOST` and `GOOUT_ELASTIC_PORT` env vars set.
- `gradle build -t` to build and test continuously ie. whenever source file changes.
- `gradle run` to launch the app. Hotswap is not supported.
- Alternatively run the Gradle tasks from Idea GUI.

## Maintenance Note

This repository is not actively maintained.
It was created using `git subtree` out of the main monorepo + a few reverts and edits.
Such approach in theory allows to bidirectionally share changes between the 2 versions, buch such activity is not expected.
