FROM openjdk:11-jre-slim

COPY ./build/libs/Locations-1.0-SNAPSHOT-all.jar /app.jar

CMD ["java", "-XX:MaxRAMPercentage=75", "-jar", "/app.jar"]
