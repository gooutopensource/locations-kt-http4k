import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    extra["kotlin_version"] = "1.3.72"
    extra["http4k_version"] = "3.163.0"
    extra["junit_version"] = "5.4.2"
    extra["log4j_version"] = "2.13.1"
}

plugins {
    application
    kotlin("jvm") version project.extra["kotlin_version"] as String
    id("org.jlleitschuh.gradle.ktlint") version("9.2.1")
    // plugin for creating fat jars
    id("com.github.johnrengelman.shadow") version "5.2.0"
}

repositories {
    maven(url = "https://jitpack.io")
    maven(url = "https://plugins.gradle.org/m2/")
}

val compileKotlin: KotlinCompile by tasks
val compileTestKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions.jvmTarget = "11"
compileTestKotlin.kotlinOptions.jvmTarget = "11"

application {
    version = "1.0-SNAPSHOT"
    mainClassName = "net.goout.locations.MainKt"
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:${project.extra["kotlin_version"]}")
    implementation("org.http4k:http4k-core:${project.extra["http4k_version"]}")
    implementation("org.http4k:http4k-server-jetty:${project.extra["http4k_version"]}")
    implementation("org.http4k:http4k-contract:${project.extra["http4k_version"]}")
    implementation("org.http4k:http4k-format-jackson:${project.extra["http4k_version"]}")
    implementation("org.apache.logging.log4j:log4j-api:${project.extra["log4j_version"]}")
    implementation("org.apache.logging.log4j:log4j-core:${project.extra["log4j_version"]}")
    implementation("org.apache.logging.log4j:log4j-slf4j-impl:${project.extra["log4j_version"]}")
    implementation("com.github.jillesvangurp:es-kotlin-wrapper-client:1.0-X-Beta-3-7.6.2")
    // JAXB API was removed in Java11, we must add it manually https://stackoverflow.com/questions/43574426/how-to-resolve-java-lang-noclassdeffounderror-javax-xml-bind-jaxbexception-in-j
    implementation("jakarta.xml.bind:jakarta.xml.bind-api:2.3.2")
    implementation("org.glassfish.jaxb:jaxb-runtime:2.3.2")
    implementation("org.webjars:swagger-ui:3.25.2")

    testImplementation("org.junit.jupiter:junit-jupiter-api:${project.extra["junit_version"]}")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:${project.extra["junit_version"]}")
    testImplementation("org.junit.jupiter:junit-jupiter-params:${project.extra["junit_version"]}")
    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.3.2")
    testImplementation("io.mockk:mockk:1.9.3")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

ktlint {
    enableExperimentalRules.set(true)
    version.set("0.37.2")
}

tasks.withType<ShadowJar> {
    // resolving missing/overridden content https://stackoverflow.com/questions/56152800/error-with-building-fatjar-using-kotlin-dsl
    mergeServiceFiles()
    manifest {
        attributes["Main-Class"] = "net.goout.locations.MainKt"
        //  multi-release class files are not being picked up from META-INF/versions/* if not set to true
        //  https://stackoverflow.com/questions/52953483/logmanager-getlogger-is-unable-to-determine-class-name-on-java-11
        attributes["Multi-Release"] = "true"
    }
}
